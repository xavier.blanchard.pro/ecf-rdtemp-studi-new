<?php

namespace App\Controller\Admin;

use App\Entity\User;
use App\Entity\Officine;
use App\Entity\ChambreFroide;
use App\Entity\DataTemp;
use App\Entity\DataHygro;

use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminDashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        return parent::index();
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('RDtemp Studi New');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::linkToCrud('Utilisateurs', 'fas fa-list', User::class);
        yield MenuItem::linkToCrud('Officines', 'fas fa-list', Officine::class);
        yield MenuItem::linkToCrud('Chambres Froides', 'fas fa-list', ChambreFroide::class);
    }
}
