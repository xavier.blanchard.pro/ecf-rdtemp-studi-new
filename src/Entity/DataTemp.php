<?php

namespace App\Entity;

use App\Repository\DataTempRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DataTempRepository::class)
 */
class DataTemp
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $DateHeure;

    /**
     * @ORM\Column(type="decimal", precision=5, scale=2)
     */
    private $Valeur;

    /**
     * @ORM\Column(type="boolean")
     */
    private $Validation;

    /**
     * @ORM\ManyToOne(targetEntity=ChambreFroide::class, inversedBy="dataTemps")
     * @ORM\JoinColumn(nullable=false)
     */
    private $ChambreFroide;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateHeure(): ?\DateTimeInterface
    {
        return $this->DateHeure;
    }

    public function setDateHeure(\DateTimeInterface $DateHeure): self
    {
        $this->DateHeure = $DateHeure;

        return $this;
    }

    public function getValeur(): ?string
    {
        return $this->Valeur;
    }

    public function setValeur(string $Valeur): self
    {
        $this->Valeur = $Valeur;

        return $this;
    }

    public function getValidation(): ?bool
    {
        return $this->Validation;
    }

    public function setValidation(bool $Validation): self
    {
        $this->Validation = $Validation;

        return $this;
    }

    public function getChambreFroide(): ?ChambreFroide
    {
        return $this->ChambreFroide;
    }

    public function setChambreFroide(?ChambreFroide $ChambreFroide): self
    {
        $this->ChambreFroide = $ChambreFroide;

        return $this;
    }
}
