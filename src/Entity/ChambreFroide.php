<?php

namespace App\Entity;

use App\Repository\ChambreFroideRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ChambreFroideRepository::class)
 */
class ChambreFroide
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Nom;

    /**
     * @ORM\ManyToOne(targetEntity=Officine::class, inversedBy="chambreFroides")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Officine;

    /**
     * @ORM\OneToMany(targetEntity=DataTemp::class, mappedBy="ChambreFroide")
     */
    private $dataTemps;

    /**
     * @ORM\OneToMany(targetEntity=DataHygro::class, mappedBy="ChambreFroide")
     */
    private $dataHygros;

    public function __construct()
    {
        $this->dataTemps = new ArrayCollection();
        $this->dataHygros = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->Nom;
    }

    public function setNom(string $Nom): self
    {
        $this->Nom = $Nom;

        return $this;
    }

    public function getOfficine(): ?Officine
    {
        return $this->Officine;
    }

    public function setOfficine(?Officine $Officine): self
    {
        $this->Officine = $Officine;

        return $this;
    }

    /**
     * @return Collection|DataTemp[]
     */
    public function getDataTemps(): Collection
    {
        return $this->dataTemps;
    }

    public function addDataTemp(DataTemp $dataTemp): self
    {
        if (!$this->dataTemps->contains($dataTemp)) {
            $this->dataTemps[] = $dataTemp;
            $dataTemp->setChambreFroide($this);
        }

        return $this;
    }

    public function removeDataTemp(DataTemp $dataTemp): self
    {
        if ($this->dataTemps->removeElement($dataTemp)) {
            // set the owning side to null (unless already changed)
            if ($dataTemp->getChambreFroide() === $this) {
                $dataTemp->setChambreFroide(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|DataHygro[]
     */
    public function getDataHygros(): Collection
    {
        return $this->dataHygros;
    }

    public function addDataHygro(DataHygro $dataHygro): self
    {
        if (!$this->dataHygros->contains($dataHygro)) {
            $this->dataHygros[] = $dataHygro;
            $dataHygro->setChambreFroide($this);
        }

        return $this;
    }

    public function removeDataHygro(DataHygro $dataHygro): self
    {
        if ($this->dataHygros->removeElement($dataHygro)) {
            // set the owning side to null (unless already changed)
            if ($dataHygro->getChambreFroide() === $this) {
                $dataHygro->setChambreFroide(null);
            }
        }

        return $this;
    }
}
