<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211018132028 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE chambre_froide (id INT AUTO_INCREMENT NOT NULL, officine_id INT NOT NULL, nom VARCHAR(255) NOT NULL, INDEX IDX_5E245AA7B2D03E4E (officine_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE data_hygro (id INT AUTO_INCREMENT NOT NULL, chambre_froide_id INT NOT NULL, date_heure DATETIME NOT NULL, valeur NUMERIC(5, 2) NOT NULL, validation TINYINT(1) NOT NULL, INDEX IDX_BA745200C621DF84 (chambre_froide_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE data_temp (id INT AUTO_INCREMENT NOT NULL, chambre_froide_id INT NOT NULL, date_heure DATETIME NOT NULL, valeur NUMERIC(5, 2) NOT NULL, validation TINYINT(1) NOT NULL, INDEX IDX_B01C4E28C621DF84 (chambre_froide_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE officine (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, raison_sociale VARCHAR(255) NOT NULL, adresse VARCHAR(255) NOT NULL, code_postal VARCHAR(255) NOT NULL, ville VARCHAR(255) NOT NULL, telephone VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_66339666A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649F85E0677 (username), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE chambre_froide ADD CONSTRAINT FK_5E245AA7B2D03E4E FOREIGN KEY (officine_id) REFERENCES officine (id)');
        $this->addSql('ALTER TABLE data_hygro ADD CONSTRAINT FK_BA745200C621DF84 FOREIGN KEY (chambre_froide_id) REFERENCES chambre_froide (id)');
        $this->addSql('ALTER TABLE data_temp ADD CONSTRAINT FK_B01C4E28C621DF84 FOREIGN KEY (chambre_froide_id) REFERENCES chambre_froide (id)');
        $this->addSql('ALTER TABLE officine ADD CONSTRAINT FK_66339666A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE data_hygro DROP FOREIGN KEY FK_BA745200C621DF84');
        $this->addSql('ALTER TABLE data_temp DROP FOREIGN KEY FK_B01C4E28C621DF84');
        $this->addSql('ALTER TABLE chambre_froide DROP FOREIGN KEY FK_5E245AA7B2D03E4E');
        $this->addSql('ALTER TABLE officine DROP FOREIGN KEY FK_66339666A76ED395');
        $this->addSql('DROP TABLE chambre_froide');
        $this->addSql('DROP TABLE data_hygro');
        $this->addSql('DROP TABLE data_temp');
        $this->addSql('DROP TABLE officine');
        $this->addSql('DROP TABLE user');
    }
}
